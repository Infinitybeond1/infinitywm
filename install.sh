#!/usr/bin/env zsh

git clone https://gitlab.com/Infinitybeond1/infinitywm
mv infinitywm ~/.config
cd ~/.config/infinitywm/chadwm
sudo make install

cd ..
sudo  mv  scripts/def-dmenu /usr/local/bin/
sudo  mv  scripts/def-ssdmenu /usr/local/bin/

